// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////
//Fichero cpp de Alejandro López - 52072
#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
	posicion.x=0;
	posicion.y=0;
	aceleracion.x=aceleracion.y=0;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro=centro+velocidad*t+aceleracion*(0.5*t*t);
	velocidad=velocidad+aceleracion*t;
}
