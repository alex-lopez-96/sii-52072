// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////
//Fichero cpp de Alejandro López - 52072
#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	aceleracion.x=0;
	//aceleracion.y=0;
	velocidad.x=0;
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	y1=y1+velocidad.y*t;
	y2=y2+velocidad.y*t;
	velocidad=velocidad+aceleracion*t;
}
